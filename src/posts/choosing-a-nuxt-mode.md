---
title: Choosing a Nuxt Mode For Your Next Web App
lang: en-US
homepage: true
meta:
  - name: date
    content: July 24th, 2018
  - name: description
    content: Pros and cons of 3 different types of web apps
  - name: keywords
    content: which nuxt mode web app spa uwa pre rendered diagram
  - name: og:image
    content: https://s3.us-east-2.amazonaws.com/recurse.me/nuxt_modes.jpeg
---

<div>
  <h1>Choosing a Nuxt Mode For Your Next Web App</h1>
  <i>July 24th, 2018</i>
  •
  <router-link to="/">
    <a>Emerson Matthews</a>
  </router-link>
</div>

<br/>

![Nuxt Modes](https://s3.us-east-2.amazonaws.com/recurse.me/nuxt_modes.jpeg)

Nuxt, one of the most popular Vue frameworks for new web apps, can greatly improve your app performance and SEO. One of the most important Nuxt configurations is the mode, as it determines how your app is built, deployed, and served.  There are three main types of web apps out there today:

1) [Classic Single-Page App (SPA)](#classic-single-page-app-spa)
1) [Universal/Isomorphic Web App (UWA)](#universal-isomorphic-web-app-uwa)
1) [Pre-Rendered Single-Page App](#pre-rendered-single-page-app)

<br/>
Nuxt makes it easy to switch between all three. We'll go over the pros and cons of each to help you decide what is best for your app.


<!-- more -->


## Classic Single-Page App (SPA)

![SPA Diagram](https://s3.us-east-2.amazonaws.com/recurse.me/spa.png)

In a classic SPA, the HTML served to the client is relatively empty, and JavaScript dynamically renders HTML once it gets to the client. Because of this, you may see a "white flicker" before the webpage is fully loaded.

You can use this mode in Nuxt with the typical `nuxt build` command and one line of configuration:
```js
// nuxt.config.js
module.exports = {
  mode: 'spa',
};
```

### Pros
- Can handle any routing situation
- Just static files, making it easy to deploy to a CDN

### Cons
- Slow initial load time
- Poor SEO, especially if content is asynchronously loaded



## Universal/Isomorphic Web App (UWA)

![SPA Diagram](https://s3.us-east-2.amazonaws.com/recurse.me/nuxt_app.png)

In a UWA, JavaScript renders the initial HTML the same way SPAs do, but now the JavaScript runs on your Nuxt server before the content is sent back to the client. This way, the client receives the rendered HTML immediately, and will behave like a classic SPA afterwards.

You can use this mode in Nuxt with the typical `nuxt build` command and one optional line of configuration:
``` js
// nuxt.config.js
module.exports = {
  mode: 'universal', // this is also the default
};
```
Now just start your server with `nuxt start`

### Pros
- Can handle any routing situation
- Fast initial load time
- Good for SEO

### Cons
- You must setup a server to run Nuxt
- High traffic may put strain on your server
- You can only query and manipulate the DOM in certain hooks ([you should avoid this anyway](https://about.gitlab.com/2017/11/09/gitlab-vue-one-year-later/))



## Pre-Rendered Single-Page App

![SPA Diagram](https://s3.us-east-2.amazonaws.com/recurse.me/pre-rendered_spa.png)

In a pre-rendered SPA, you render each of your routes during your _build_ step. Your static deployed files will include pre-rendered HTML for each route, so no matter which route the client initially loads, they will receive the full content immediately, and the app will behave like a classic SPA afterwards.

Nuxt will only automatically do this for static routes. If you have any dynamic routes such as `users/{userId}` then you will need to inform Nuxt what the actual routes are so that it may "navigate" to that route and render it:
``` js
// nuxt.config.js
const axios = require('axios');

module.exports = {
  generate: {
    routes: () => axios.get('https://my-api/users')
      .then(res => res.data.map((user) => `/users/${user.id}`)),
  },
};
```
Then, just use the `nuxt generate` command. Learn more about building for dynamic routes [here](https://nuxtjs.org/api/configuration-generate#routes)

### Pros
- Just static files, making it easy to deploy to a CDN
- Fast initial load time
- Good for SEO

### Cons
- The more routes you have, the longer your build step will be
- You can only query and manipulate the DOM in certain hooks ([you should avoid this anyway](https://about.gitlab.com/2017/11/09/gitlab-vue-one-year-later/))



## Further Information
You should now be able to infer intuitively when and where code in a [Nuxt asyncData](https://nuxtjs.org/guide/async-data) function will run for each mode, but feel free to test your conclusions with console logging!


Learn more about Nuxt and Vue SSR here:
- [https://nuxtjs.org/](https://nuxtjs.org/)
- [https://ssr.vuejs.org/](https://ssr.vuejs.org/)
