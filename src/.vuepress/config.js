module.exports = {
  title: 'recurse.me',
  description: 'A place for my random bits of thought and code',
  head: [
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Flamenco|Open+Sans|Source+Code+Pro&display=swap' }],
    ['script', { src: 'https://kit.fontawesome.com/98f5c806e2.js', crossorigin: 'anonymous' }],
    ['meta', { property: 'og:type', content: 'blog' }],
  ],
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'About', link: '/about' },
      { text: 'More', items: [
          { text: 'GitHub', link: 'https://github.com/dalematthews' },
          { text: 'GitLab', link: 'https://gitlab.com/emersonmatthews' },
          // { text: 'RSS', link: '/feed.xml' },
        ],
      },
    ],
  },
  plugins: [
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-119267117-1',
      },
    ],
  ],
};
